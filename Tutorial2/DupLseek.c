#include <stdio.h>
#include <unistd.h>

#include "Util.h"

int main(int numberOfParam, char** params)
{
    int fd_original = 0;
    int fd_duplicate = 0;
    char* file_path = params[1];
    int amount_written = 0;
    char* to_write_dup = "\n duplicate \n";
    char* to_write = "first one \n";

    int current_offset = 0;

    printf("%s \n", params[0]);

    //open file
    if((fd_original = open(file_path, O_RDWR)) < 0)
        printf("problem while opening file");


    //refer to dup2
    if((fd_duplicate = dup(fd_original)) < 0)
        printf("problem while duplicatijg file descriptor \n");

    //check all the other whence values
    //mov8ing to the offset of the end of file
    lseek(fd_original, 1, SEEK_END);
        //printf("Error during lseek issue");

    //write to file using original descriptor
    WriteToFile(fd_original, to_write, sizeof(to_write));
    WriteToFile(fd_duplicate, to_write_dup, sizeof(to_write_dup));

    close(fd_original);
    close(fd_duplicate);

    return 0;
}
