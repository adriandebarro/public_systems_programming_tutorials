Funny quotes are great for bringing a bit of humor into our lives. They help to lighten the mood, they’re excellent for breaking the ice, and of course, they’re fun to share with friends. After all, as Victor Borge once said, “Laughter is the shortest distance between two people.”

Some of the most entertaining quotes, in our opinion, are those that are not only witty, but also wise — conveying some universal truth. Funny quotes about life, love, and friendship — those that we can relate to — are especially comical. Kurt Vonnegut once quipped, “The best jokes are dangerous, and dangerous because they are in some way truthful.”

In this list of 100 funny quotes worth laughing over, we’ve tried to include a zinger for everyone, whether it’s an insightful quotation, a silly saying, or an ironic wisecrack....
first on
 duplicfirst on
 duplic first on
 duplic first on
 duplic