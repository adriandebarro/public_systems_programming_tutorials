#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>


static void WriteToFile(int p_file_descriptor, char* buffer, size_t p_buffer_size)
{
	int amountWritten = 0;

	if((amountWritten = write(p_file_descriptor, buffer, p_buffer_size)) != p_buffer_size)
		printf("Write operation failed on file descriptor %d", p_file_descriptor);
}

static int ReadFile(int p_file_descriptor, char* p_buffer_out, size_t p_buffer_size)
{
	int amountRead = 0;
	if((amountRead = read(p_file_descriptor, p_buffer_out, p_buffer_size)) < 0)
		printf("Error while reading file descriptor %d", p_file_descriptor);
}
