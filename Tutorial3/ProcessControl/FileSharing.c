#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include <sys/wait.h>

/*
 *Filedescriptors are inherited by the child processes thus shared by the parent and child
 */
int main(void)
{
    FILE *f = fopen("shared_file.txt", "w");
    int status = 0;
    pid_t tempPid;
    char* parentSpeech = "Process! ... I am your father! \n";
    char* childSpeech = "NOOOOOOOOO!!! \n";

    if(f == NULL)
    {
        printf("File could not be opened \n");
        return 0;
    }
    else
    {
        pid_t pid = fork();

        if(pid  < 0)
        {
            printf("we have a problem in the fork \n");
        }
        else if(pid == 0)
        {
            fputs(childSpeech, f);
            printf("child process denied his father \n");
        }
        else
        {
            fputs(parentSpeech, f);
            while(tempPid = wait(&status) != pid);
            printf("Parent process ready \n");
        }
    }

    return 0;
}
