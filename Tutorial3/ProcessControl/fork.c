#include<stdio.h>
#include<unistd.h>

void PrintChild(void)
{
	printf("this is a child process \n");
}

void PrintParent(void)
{
	printf("this is the parent \n");
}


/*
 *The fork creates a new process 
 *Difference between process and program?
 */
int main(void)
{
	printf("this iS the parent process \n");
	//fork process returns 0 for the child and the process id of child process to the parent
	pid_t pid = fork();

	//if pid is smaller than 0 we might have have a problem...
	if (pid < 0) 
	{
		printf("we have a problem");
	}
		//the child process receives a pid  equal to 0
	else if (pid == 0) {
		//sleep(2);
		pid_t childPid = getpid();
		PrintChild();
		printf("i am the child process with process id %d \n", childPid);
	}
		//while the parent receives the pid of the child process
	else
	{
		sleep(4);
		PrintParent();
		printf("I am the parent process and my child has a process id of %d \n", pid);
	}

	return 0;
}

