#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main(void)
{
	int temp = 0;
	printf("this example will be executing a system call");

	//Call System functions
	temp  = system("ls");

	//Call exec to initiate a new program
	//known as the exec family (pg 231)
	execl("./Fork.out","", (char *) 0);

	return 0;
}
