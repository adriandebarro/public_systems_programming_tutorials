#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(void)
{

	int tester = 0, status;
	pid_t tempPid;
	//vfork allows the child and parent to share the same address space
	pid_t pid = vfork();

	if(pid < 0)
	{
		printf("we have an issue ");
	}
	else if(pid == 0)
	{
		tester ++;
		//after executing the child process should call the exit function
		//_exit does not flush standard io or close and handlers
		//there are 5 ways through which a process can exit 
		_exit(0);
	}
	else
	{
		//spinlock not ideal why?
		//there exist multiple types of wait functions some of which can be set to be non blocking
		//the wait also reaps the pid of the child process from the process table 
		//if not reaped the child process can be called as a zombie process
		while(tempPid = wait(&status) != pid);
		printf("Parent : %d \n", tester);
	}

	return 0;
}
