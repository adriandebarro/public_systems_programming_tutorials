#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#define USE_IGNORE 0

// -SIGSTOP to stop process
// -SIGCONT to continue process

// Global signal handler
void signal_handler(int sig)
{
    if (sig == SIGINT)
	printf("Nice try...\n");
    else
	printf("Received signal %d which shouldn't be handled by this handler\n", sig);
}

int main()
{
    int i = 0;

    // Capture SIGINT signals
    #if USE_IGNORE
       if (signal(SIGINT, SIG_IGN) == SIG_ERR)
           fprintf(stderr, "Can't catch SIGINT\n");
    #else
       if(signal(SIGINT, signal_handler) == SIG_ERR)
           fprintf(stderr, "Can't catch SIGINT\n");
    #endif

    // Required number of seconds
    int seconds = 5;

    while (1)
    {
         printf("Hello %d\n", i++);

	 // When SIGINT is received, the following function call will
         // fail with EINTR (interrupted). We have to loop over the 
         // time period to make sure that the required number of seconds 
	 // elapse
                               
         int remaining = seconds;
         do
	 {
             int ret = sleep(remaining);
             remaining = (ret == 0) ? 0 : remaining - ret;
         }

         while (remaining > 0);
    }
}
