#include <stdio.h>
#include <signal.h>
#include <unistd.h>

// Global signal handler
void signal_handler( int sig ) 
{
    printf("signal_handler() has gained control\n");
}

int main( int argc, char *argv[] ) 
{
    struct sigaction sigact;
    sigset_t sigset;

    sigemptyset(&sigact.sa_mask );
    sigact.sa_flags = 0;
    sigact.sa_handler = signal_handler;
    sigaction( SIGUSR1, &sigact, NULL );

    printf("Before first kill()\n" );
    kill(getpid(), SIGUSR1 );

    /*
     * Blocking SIGUSR1 signals prevents the signals
     * from being delivered until they are unblocked,
     * so the catcher will not gain control.
     */

    sigemptyset(&sigset );
    sigaddset(&sigset, SIGUSR1 );
    sigprocmask(SIG_SETMASK, &sigset, NULL );

    printf("Before second kill()\n" );
    kill( getpid(), SIGUSR1 );
    printf("After second kill()\n" );

    sleep(2);

    /*
     * Calling sigprocmask to unblock a signal
     * will cause SIGUSR1 to be delivered and the
     * the signal handler to be called before
     * sigprocmask returns
     */

    printf("After sleep\n");
    sigprocmask(SIG_UNBLOCK, &sigset, NULL);
    printf("After unblocking signal\n");

    return 0;
}
