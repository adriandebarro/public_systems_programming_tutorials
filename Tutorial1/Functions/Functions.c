#include <stdio.h>
#include <stdlib.h>

/*
 * prototyping example
 */
int Addition(int, int);

/*
 * Subtraction function
 */
int Subtraction(int p_var1, int p_var2)
{
    return p_var1 - p_var2;
}

typedef void (*CallbackFunction)(int a, int b, int c);

void Print(int p_x, int p_y, int p_z)
{
    printf("%d, %d, %d \n", p_x, p_y, p_z);
}

void ExecuteMethodAfterWait(int p_x, int p_y, int p_z, CallbackFunction p_callbackFunction )
{
    int index = 0;
    _sleep(2000);
    p_callbackFunction(p_x, p_y, p_z);
}

/*
 * Main method
 */
int main(void)
{
    int add = Addition(5, 5);
    int sub = Subtraction(5, 5);
    printf("The addition is equal %d \n", add);
    printf("The subtraction is equal to %d \n", sub);

    ExecuteMethodAfterWait(1,2,3,Print);
    return 0;
}

/*
 * Addition function prototyped
 */
int Addition(int p_var1, int p_var2)
{
    return p_var1 + p_var2;
}
