#include <stdio.h>


int main(void)
{
    /*
     * Variables always declared at the top of the function
     * */

    char character = 'a';
    int integer = 5;
    float floatNumber = 5.f;
    double doubleNumber = 6.f;
    char charArray[10] = {'a'};
    int index = 0, sizeArray = 0;
    float average = 0.f;
    float average2 = 0;
    int sum = 0;

    sum = 10 + 22+ 30;

    average = (float) sum / 3.f;

    printf(" average is %f\n", average);

    printf(" This is the inputted character %c \n", character);
    printf(" This is the inputted character %d \n", integer);
    printf(" This is the inputted float %f \n", floatNumber);
    printf(" This is the inputted double %lf \n", doubleNumber);
    printf(" This is the inputted character Array %s \n", charArray);

    //number of elements in the character array
    sizeArray = sizeof(charArray)/ sizeof(char);

    //incrementing the character variable and assigning it to an index of the array
    for(index = 0; index < sizeArray; index++, character++)
    {
        charArray[index] = character;
    }

    //looping through the arra using a for loop
    for(index = 0; index < sizeArray; index++)
    {
        printf("I can print any character oin the array %c \n", charArray[index]);
    }

    index = 0;

    printf("Using the while loop \n");

    while(index < 10)
    {
        printf("%c", charArray[index]);
        index++;
    }

    printf("\n \nUsing the do while loop \n");
    index = 0;

    do
    {
        printf("%c", charArray[index]);
        index++;
    }
    while(index < 10);

    printf("\n");

    return 0;
}