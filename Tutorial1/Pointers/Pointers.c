#include <stdio.h>
#include <stdlib.h>

const int ARRAY_SIZE = 10;

void ModifyArray(char *, int *, int);

/*
 * http://www.thegeekstuff.com/2011/12/c-pointers-fundamentals/
 * For more revision follow the following link
 */

int main(void)
{
    //Variables (pointer declarations)
    char* characterPtr;
    int *integerPtr;
    int index = 0;
    char tempChar = 'a';

    //Initializing variables
    //being allocated on the heap
    characterPtr =  malloc(ARRAY_SIZE * sizeof(char));
    //integerPtr = malloc(sizeof(int));

    integerPtr =  &index;

    printf("value stored by 'integerPtr' is  %d \n", integerPtr);
    printf("value shown by dereferencing pointer is %d \n", *integerPtr);
    index++;
    printf("we can see that the value shown is infact the value stored by variable index %d \n", *integerPtr);
    printf("The value of characterPtr is  %c \n", *characterPtr);

    for(index = 0; index < ARRAY_SIZE; index++, tempChar++)
    {
        //if(index % 2 == 0)
        characterPtr[index] = tempChar;
        //(characterPtr+ (sizeof(char)* index)) = tempChar;
    }


    for(index = 0; index < 10; index ++)
    {
        printf("%c", characterPtr[index]);
    }

    printf("\n");

   // printf("The first postion is %d, A pointer always stores the starting address", &integerPtr);

    //free
   // free(integerPtr);
    free(characterPtr);

    //printf("%d", *integerPtr);
    //printf("%c", *characterPtr);
    return 0;
}