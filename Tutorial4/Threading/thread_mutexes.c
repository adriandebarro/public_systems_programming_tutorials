#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

int counter;
pthread_mutex_t lock;

// Thread function
void* doSomeThing(void *arg)
{
    // Lock mutex
    pthread_mutex_lock(&lock);

    unsigned long i = 0;
    counter += 1;
    printf("Job %d started\n", counter);

    // Sleep for a bit
    sleep(1);

    printf("Job %d finished\n", counter);

    // Unlock mutex
    pthread_mutex_unlock(&lock);

    return NULL;
}

// Main function
int main(void)
{
    pthread_t tid[2];
    int i = 0;
    int err;

    // Initialise mutex
    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("Mutex init failed\n");
        return 1;
    }

    // Create two threads
    while(i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
        if (err != 0)
            printf("Can't create thread :[%s]", strerror(err));
        i++;
    }

    // Wait for threads to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    
    // Destory mutex
    pthread_mutex_destroy(&lock);

    return 0;
}
