#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>

void *infiniteThread(void *arg)
{
	pthread_t tid = pthread_self();
	
	while(1)
	{
		//will loop forever printing the following message
		printf("this is a secondary annoying thread %u \n", (unsigned int)tid);
		sleep(1);
	}
}

int main(void)
{
	pthread_t secondaryThread;
	pthread_create(&secondaryThread, NULL, infiniteThread, NULL);
	sleep(10);
	pthread_cancel(secondaryThread);
	printf("annoying thread killed \n");
	return 0;
}


