#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define THREADS 10

// Function executed by threads
void *function(void *arg)
{
    int thread_num = *((int *) arg);
    pid_t pid = getpid();
    pthread_t tid = pthread_self();

    printf("Thread %d running with a pid %d and thread id %u \n", thread_num, pid, (unsigned int)tid);
}

// Main function
int main(void)
{	
	int i=0 ;
    // Store thread IDs
    pthread_t tids[THREADS];

    // Create arguments
//    int args[THREADS];
//    for(unsigned i = 0; i < THREADS; i++)
//        args[i] = i;

    // Create threads
    for( i= 0; i < THREADS; i++)
        pthread_create(&tids[i], NULL, function, &i);

    printf("Main thread\n");

    // Wait for threads to finish
    for(i = 0; i < THREADS; i++)
    {
        void *tret;
        pthread_join(tids[i], &tret);
    }

    printf("All done\n");

    // All done
    return 1;
}
