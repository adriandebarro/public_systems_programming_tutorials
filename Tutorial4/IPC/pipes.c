#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
	
	pid_t pid;
	int fb[2];
	char line[100];
	int n = 0;

	if(pipe(fb) < 0)
		printf("pipe was not created successfully \n");
	else
	{
		if((pid = fork()) < 0)
			printf("we have an error \n");
		else if(pid == 0)
		{
			//child process
			close(fb[1]);
			n = read(fb[0], line, 100);
			printf("read %s....NOOOOO\n", line);
		}	
		else if(pid > 0)
		{
			//parent process
			close(fb[0]);
			write(fb[1], "I am your father",16 );
			wait(&pid);
			printf("my work is done now i can exit in peace \n");	
		}
		else
			printf("we have another error");

	}	
	return 0;
}
