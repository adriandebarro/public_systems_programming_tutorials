
#include "apue.h"
#include <fcntl.h>

#define	FIFO	"temp.fifo"

void
clr_fl(int fd, int flags)
				/* flags are the file status flags to turn off */
{
	int		val;

	if ((val = fcntl(fd, F_GETFL, 0)) < 0)
		printf("fcntl F_GETFL error");

	val &= ~flags;		/* turn flags off */

	if (fcntl(fd, F_SETFL, val) < 0)
		printf("fcntl F_SETFL error");
}

int
main(void)
{
	int		fdread, fdwrite;

	unlink(FIFO);
	if (mkfifo(FIFO, FILE_MODE) < 0)
		printf("mkfifo error");
	if ((fdread = open(FIFO, O_RDONLY | O_NONBLOCK)) < 0)
		printf("open error for reading");
	if ((fdwrite = open(FIFO, O_WRONLY)) < 0)
		printf("open error for writing");
	clr_fl(fdread, O_NONBLOCK);
	exit(0);
}
