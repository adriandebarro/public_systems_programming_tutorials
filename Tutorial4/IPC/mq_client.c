#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// Message structure
struct my_msgbuf 
{
    long mtype;
    char mtext[200];
};

int main(void)
{
    struct my_msgbuf buf;
    int msqid;
    key_t key;

    // Get key from filename
    if ((key = ftok("mq_server.c", 'B')) == -1) {  /* same key as mq_server.c */
        perror("ftok");
        exit(1);
    }

    // Get message queue ID / connect to the queue
    if ((msqid = msgget(key, 0644)) == -1) { /* connect to the queue */
        perror("msgget");
        exit(1);
    }
    
    printf("Client: ready to receive messages\n");

    // Forvever
    for(;;) 
    {
        // Receive message from message queue
        if (msgrcv(msqid, &buf, sizeof(buf.mtext), 0, 0) == -1) 
        {
            perror("msgrcv");
            exit(1);
        }
        printf("Client: \"%s\"\n", buf.mtext);
    }

    return 0;
}
