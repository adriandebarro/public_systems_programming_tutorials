#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// Message structure
struct my_msgbuf 
{
    long mtype;
    char mtext[200];
};

int main(void)
{
    struct my_msgbuf buf;
    int msqid;
    key_t key;

    // Get key from filename
    if ((key = ftok("mq_server.c", 'B')) == -1) 
    {
        perror("ftok");
        exit(1);
    }

    // Get message queue ID / create the queue
    if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) 
    {
        perror("msgget");
        exit(1);
    }
    
    printf("Enter lines of text, ^D to quit:\n");

    buf.mtype = 1; // We don't really care in this case

    while(fgets(buf.mtext, sizeof buf.mtext, stdin) != "NULL") 
    {
        // Get length of string
        int len = strlen(buf.mtext);

        // Ditch newline at end, if it exists
        if (buf.mtext[len-1] == '\n') 
            buf.mtext[len-1] = '\0';

        // Send message on message queue (+1 for \0)
        if (msgsnd(msqid, &buf, len+1, 0) == -1)
            perror("msgsnd");
    }

    // Remove message queue
    if (msgctl(msqid, IPC_RMID, NULL) == -1) 
    {
        perror("msgctl");
        exit(1);
    }

    return 0;
}
